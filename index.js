let students = [];
let sectionedStudents = [];

function addStudent(name) {

    students.push(name);

    console.log(name + " was added to the student's list.");

}

	addStudent("Elisha");
	addStudent("Elisha");
	addStudent("Jude");
	addStudent("Jaq");
	addStudent("Jammie")
	
function countStudents() {

    console.log('There are a total of ' + students.length + ' students enrolled.');

}

	countStudents();

function printStudents() {
    students.sort();
    students.forEach(function(student) {
    	console.log(student);
    })

}
	printStudents();

function findStudent(keyword) {

    
    let match = students.filter(function(student) {
        return student.toLowerCase().includes(keyword.toLowerCase());
    })

    if (match.length == 1) {
        console.log(match + ' is an Enrollee');
    } else if (match.length > 1) {
        console.log(match + ' are enrollees');
    } else {
        console.log('No student found with the name ' + keyword);
    }
	}
	findStudent("elisha");


function addSection(section) {
    sectionedStudents = students.map(function(student){
        return student + ' - section ' + section; 
    })

    console.log(sectionedStudents);
}
	addSection("Sampaguita");

function removeStudent(name) {

 
    let firstLetter = name.slice(0, 1).toUpperCase();
   
    let remainingLetters = name.slice(1, name.length);

    let capitalizedName = firstLetter + remainingLetters; 

    let studentIndex = students.indexOf(capitalizedName);

    if(studentIndex >= 0) {

        
        students.splice(studentIndex, 1);
        
    }

    console.log(name + " was removed from the student's list.");

}
	findStudent("Jaq")
	findStudent("Elisha")
	findStudent("Jacob")

